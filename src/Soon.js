import { Fragment } from "react"

import "./Soon.css";

export const SoonPopup = ({onClick}) => {

    return (<Fragment>
        <div className="soon-overlay container">
            <div className="row justify-content-center pt-6 pt-md-soon pb-0 mb-2">
                <div className="col-7">
                    <div className="card card-secondary">
                        <div className="card-header text-center">
                            <span className="icon icon-xs w95-window-empty"></span>
                            SaikoCTF is Coming Soon to HITBSecConf
                        </div>
                        <div className="card-body">
                            <p className="card-text">Check back soon for SaikoCTF registration.</p>
                            <div className="d-flex mt-3">
                                <button onClick={onClick} className="btn btn-sm mr-2 btn-primary border-dark"
                                    type="button"><span className="btn-text">OK</span></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </Fragment>)

}