import "bootstrap/dist/css/bootstrap.min.css";
import './App.css';

import { Fragment, useEffect, useState } from "react";

import "jquery";
import "bootstrap";

import dayjs from "dayjs";
import Duration from "dayjs/plugin/duration";
import { SoonPopup } from "./Soon";


const dayjsSetup = () => {
  dayjs.extend(Duration);
}

const countdown_until = dayjs("2024-08-25T08:00:00");


function App() {
  
  dayjsSetup();

  const [curTime, setCurTime] = useState("0.0.0:00:00:00");
  const [soonState, setSoonState] = useState(false);

  function formatTimeUntil(until_date) {
      let now = dayjs()
      let fmt = dayjs.duration(until_date.diff(now)).format("M.D.H:mm:ss");

      setCurTime(fmt)
  }

  
  useEffect(() => {
    formatTimeUntil(countdown_until);
    var id = setInterval(formatTimeUntil, 1000, countdown_until);

    return () => {
      clearInterval(id);
    }
  })

  var primaryWindowState = "card-tertiary";
  if (soonState) {
    primaryWindowState = "";
  }

  const handleSoonClick = () => {
    setSoonState(!soonState);
  }

  return (
    <Fragment>
      <main className="bg-secondary z-2">
              <div className="container z-2">
                  <div className="row justify-content-center pt-6 pt-md-5 pb-0 mb-2">
                      <div className="col-7">


                          <div className={`card ${primaryWindowState}`}>
                              <div className="card-header text-center">
                                  <span>SaikoCTF Coming Soon</span>
                              </div>
                              <div className="card-body">
                                      <img className="w-100 h-100"  src="/img/kanji-v2.png" alt="SaikoCTF"/>
                              </div>
                          </div>


                      </div>
                  </div>
              </div>
              { soonState && <SoonPopup onClick={handleSoonClick}/> }
      </main>
      <footer>
          <nav id="navbar-footer" className="navbar navbar-main navbar-expand-lg navbar-dark justify-content-between navbar-footer">
              <ul className="navbar-nav navbar-nav-hover flex-row align-items-center">
                  <li className="nav-item">
                      <a href="#coming-soon" onClick={handleSoonClick} className="nav-link" role="button">
                          <span className="nav-link-inner-text">&#x1F6A7; Coming Soon</span>
                      </a>
                  </li>
              </ul>
              <div className="time text-center text-uppercase">
                  {curTime}
              </div>
          </nav>
      </footer>
    </Fragment>
  );
}

export default App;
